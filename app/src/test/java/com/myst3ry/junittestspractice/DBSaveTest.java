package com.myst3ry.junittestspractice;

import com.myst3ry.junittestspractice.db.DBManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

@RunWith(RobolectricTestRunner.class)
@Config(manifest = Config.NONE)
public class DBSaveTest {

    private DBManager mDBManager;

    @Before
    public void setUp() {
        mDBManager = DBManager.getInstance(RuntimeEnvironment.application);
    }

    @Test
    public void testCorrectSaveIntoDb() {
        final int expectedResult = 7;
        mDBManager.saveResult(expectedResult);
        final int actualResult = mDBManager.getResult();
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testIncorrectSaveIntoDb() {
        final int expectedResult = 7;
        final int wrongResult = 8;
        mDBManager.saveResult(wrongResult);
        final int actualResult = mDBManager.getResult();
        assertNotEquals(expectedResult, actualResult);
    }
}