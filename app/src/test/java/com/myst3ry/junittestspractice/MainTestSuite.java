package com.myst3ry.junittestspractice;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        CalculatorTest.class,
        DBSaveTest.class})

public class MainTestSuite {
}