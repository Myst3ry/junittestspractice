package com.myst3ry.junittestspractice;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.fail;

public class CalculatorTest {

    private static final String PRIVATE_METHOD_NAME = "add";

    private static Calculator mCalculator;

    @BeforeClass
    public static void setUp() {
        mCalculator = new Calculator();
    }

    @AfterClass
    public static void clearCalculator() {
        mCalculator = null;
    }

    @Test
    public void testEqualsDivision() {
        assertEquals(-4, Calculator.calculate(8, -2, OperatorType.DIVIDE));
    }

    @Test
    public void testNotEqualsDivision() {
        assertNotEquals(8, Calculator.calculate(4, 2, OperatorType.DIVIDE));
    }

    @Test(expected = java.lang.ArithmeticException.class)
    public void testDivisionByZero() {
        assertNotEquals(5, Calculator.calculate(2, 0, OperatorType.DIVIDE));
    }

    @Test
    public void testEqualsMultiplication() {
        assertEquals(-24, Calculator.calculate(-6, 4, OperatorType.MULTIPLY));
    }

    @Test
    public void testNotEqualsMultiplication() {
        assertNotEquals(16, Calculator.calculate(2, 6, OperatorType.MULTIPLY));
    }

    @Test
    public void testEqualsSubtraction() {
        assertEquals(-5, Calculator.calculate(-1, 4, OperatorType.SUBTRACT));
    }

    @Test
    public void testNotEqualsSubtraction() {
        assertNotEquals(7, Calculator.calculate(5, 6, OperatorType.SUBTRACT));
    }

    @Test
    public void testEqualsAddition() {
        assertEquals(3, Calculator.calculate(-1, 4, OperatorType.ADD));
    }

    @Test
    public void testNotEqualsAddition() {
        assertNotEquals(8, Calculator.calculate(3, 4, OperatorType.ADD));
    }

    @Test
    public void testEqualsPrivateAddition() {
        try {
            Method method = mCalculator.getClass().getDeclaredMethod(PRIVATE_METHOD_NAME, Integer.class, Integer.class);
            method.setAccessible(true);
            assertEquals(18, (int) method.invoke(mCalculator, 12, 6));
            method.setAccessible(false);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            fail(e.getLocalizedMessage());
        }
    }

    @Test
    public void testNotEqualsPrivateAddition() {
        try {
            Method method = mCalculator.getClass().getDeclaredMethod(PRIVATE_METHOD_NAME, Integer.class, Integer.class);
            method.setAccessible(true);
            assertNotEquals(10, (int) method.invoke(mCalculator, 5, 6));
            method.setAccessible(false);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            fail(e.getLocalizedMessage());
        }
    }
}
