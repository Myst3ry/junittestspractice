package com.myst3ry.junittestspractice.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public final class DBHelper extends SQLiteOpenHelper {

    private final static int DB_VERSION = 1;
    private final static String DB_NAME = "calculator_db";

    public DBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createTables(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        dropTables(db);
        createTables(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    private void createTables(final SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + DBContract.CALC_RES_TABLE_NAME + " (" + DBContract.COLUMN_TEMP_RES + " INTEGER)");
    }

    private void dropTables(final SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + DBContract.CALC_RES_TABLE_NAME);
    }
}
