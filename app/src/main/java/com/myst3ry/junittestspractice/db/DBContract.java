package com.myst3ry.junittestspractice.db;

public final class DBContract {

    public static final String CALC_RES_TABLE_NAME = "calculator_result";
    public static final String COLUMN_TEMP_RES = "temp_result";

    private DBContract() {
    }
}
