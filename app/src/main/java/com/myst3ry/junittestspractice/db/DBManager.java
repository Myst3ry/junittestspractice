package com.myst3ry.junittestspractice.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

public final class DBManager {

    private static final String TAG = "DBManager";

    private DBHelper mHelper;
    private SQLiteDatabase mDatabase;

    private static volatile DBManager INSTANCE;

    public static DBManager getInstance(final Context context) {
        DBManager instance = INSTANCE;
        if (instance == null) {
            synchronized (DBManager.class) {
                instance = INSTANCE;
                if (instance == null) {
                    instance = INSTANCE = new DBManager(context);
                }
            }
        }
        return instance;
    }

    private DBManager(final Context context) {
        mHelper = new DBHelper(context);
    }

    public int getResult() {
        int result = 0;
        try {
            mDatabase = mHelper.getReadableDatabase();
            mDatabase.beginTransaction();

            final Cursor cursor = mDatabase.query(DBContract.CALC_RES_TABLE_NAME, null, null,
                    null, null, null, null);
            if (cursor.moveToLast()) {
                result = cursor.getInt(cursor.getColumnIndex(DBContract.COLUMN_TEMP_RES));
                Log.w(TAG, String.valueOf(result));
            }
            cursor.close();

            mDatabase.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
            Log.e(TAG, e.getLocalizedMessage());
        } finally {
            if (mDatabase != null) {
                if (mDatabase.inTransaction()) {
                    mDatabase.endTransaction();
                }
                mDatabase.close();
                mDatabase = null;
            }
        }
        return result;
    }

    public void saveResult(final int result) {
        try {
            mDatabase = mHelper.getReadableDatabase();
            mDatabase.beginTransaction();

            final ContentValues values = new ContentValues();
            values.put(DBContract.COLUMN_TEMP_RES, result);

            mDatabase.insert(DBContract.CALC_RES_TABLE_NAME, null, values);
            mDatabase.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
            Log.e(TAG, e.getLocalizedMessage());
        } finally {
            if (mDatabase != null) {
                if (mDatabase.inTransaction()) {
                    mDatabase.endTransaction();
                }
                mDatabase.close();
                mDatabase = null;
            }
        }
    }

    public void removeResult() {
        try {
            mDatabase = mHelper.getReadableDatabase();
            mDatabase.beginTransaction();
            mDatabase.delete(DBContract.CALC_RES_TABLE_NAME, null, null);
            mDatabase.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
            Log.e(TAG, e.getLocalizedMessage());
        } finally {
            if (mDatabase != null) {
                if (mDatabase.inTransaction()) {
                    mDatabase.endTransaction();
                }
                mDatabase.close();
                mDatabase = null;
            }
        }
    }
}
