package com.myst3ry.junittestspractice;

public final class Calculator {

    private Calculator() {
    }

    public static int calculate(int value1, int value2, final OperatorType operator) {
        int result = 0;

        switch (operator) {
            case ADD:
                result = add(value1, value2);
                break;
            case SUBTRACT:
                result = subtract(value1, value2);
                break;
            case MULTIPLY:
                result = multiply(value1, value2);
                break;
            case DIVIDE:
                result = divide(value1, value2);
                break;
            default:
                break;
        }
        return result;
    }

    private static int add(final Integer value1, final Integer value2) {
        return value1 + value2;
    }

    public static int subtract(final int value1, final int value2) {
        return value1 - value2;
    }

    public static int multiply(final int value1, final int value2) {
        return value1 * value2;
    }

    public static int divide(final int value1, final int value2) {
        return value1 / value2;
    }
}
