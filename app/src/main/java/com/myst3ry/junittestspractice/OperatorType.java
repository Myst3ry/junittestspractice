package com.myst3ry.junittestspractice;

public enum OperatorType {
    ADD, SUBTRACT, MULTIPLY, DIVIDE
}