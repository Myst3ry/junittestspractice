package com.myst3ry.junittestspractice;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.myst3ry.junittestspractice.db.DBManager;


public final class MainActivity extends AppCompatActivity {

    private DBManager mDBManager = DBManager.getInstance(this);

    private TextView resultTextView;

    private int mResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();
        calcTest();
        updateUI();
    }

    private void initViews() {
        resultTextView = findViewById(R.id.text_result);
    }

    private void calcTest() {
        mResult = Calculator.calculate(getLastResult(), 9, OperatorType.ADD);
        saveResult(mResult);
        mResult = Calculator.calculate(getLastResult(), 3, OperatorType.MULTIPLY);
        saveResult(mResult);
    }

    private void updateUI() {
        resultTextView.setText(String.valueOf(mResult));
    }

    private int getLastResult() {
        return mDBManager.getResult();
    }

    private void saveResult(final int result) {
        mDBManager.saveResult(result);
    }
}
